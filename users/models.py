# username for this project -> yash5

from django.db import models
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    age = models.PositiveIntegerField(null=True, blank=True)


'''
AbstractBaseUser vs AbstractUser

AbstractBaseUser requires a very fine level of control and customization.
We essentially rewrite Django. This can be helpful, but if we just want a
custom user model that can be updated with additional fields, the better
choice is AbstractUser which subclasses AbstractBaseUser.

In other words, we write much less code and have lessopportunity to mess
things up. It’s the better choice unless you really know whatyou’re doing
with Django!
'''
