
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser


class CustomUserAdmin(UserAdmin):
    add_form     = CustomUserCreationForm
    form         = CustomUserChangeForm
    model        = CustomUser
    list_display = ['email', 'username', 'age', 'is_staff']   # fields to be display on admin page

admin.site.register(CustomUser, CustomUserAdmin)




'''
The only other step we need is to update our admin.py file since Admin is tightly
coupled to the default User model. We will extend the existing UserAdmin class to
use our new CustomUser model.
'''