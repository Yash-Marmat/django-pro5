
from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm  # summary below
from .models import CustomUser

class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):                              # summary below
        model  = CustomUser
        fields = UserCreationForm.Meta.fields + ('age',)      # don't forget this comma

class CustomUserChangeForm(UserChangeForm):
    class Meta:                                 
        model  = CustomUser
        fields = UserChangeForm.Meta.fields





''' why using UserCreationForm and UserChangeForm ?

fields on UserCreationForm is just username, email, and password even though there are
many more fields available.

whereas the UserChangeForm is use in the admin interface to change a user’s information 
and permissions.

'''

'''
For both new forms we are setting the model to our CustomUser and using the default
fields via Meta.fields which includes all default fields. To add our custom age field
we simply tack it on at the end and it will display automatically on our future sign up
page.

'''