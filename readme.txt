-this project is based on custom user model
-so instead of using the models.Model we are using AbstractUser
-also mentioned the custom user model -> AUTH_USER_MODEL = 'users.CustomUser' in the settings file in bottom
-where users is the name of our app.
-usually most of the models work now done in forms.py file and the main models.py file work on
 custom user model with a single field.

-format followed -> models.py, forms.py, admin.py.

-Why using custom user model ?

The reason is if you want to make any changes to the User model down the road–for example adding a date of birth field–using a custom user model from the beginning makes this quite easy. But if you do not, updating the default User model in an existing Django project is very, very challenging.

similarly in this project we mentioned other fields (like username, age, email etc.) later means after creating a model file and assigned those new fields in forms.py file.


